template <class T>
class Array
{
protected:
    T* data;
    unsigned int base;
    unsigned int length;
public:
    Array () :data (new T [0]),base (0),length (0)
    {
    }
    Array (unsigned int n, unsigned int m) :data (new T [n]),base (m),length (n)
    {
    }
    ~Array ()
    {
        delete [] data;
    }

    Array (Array<T> const& array) :data (new T [array.length]), base (array.base),length (array.length)
    {
    for (unsigned int i = 0; i < length; ++i)
	data [i] = array.data [i];
    }

    T const* Data () const
    {
         return data;
    }
    unsigned int Base () const
    {
        return base;
    }
    unsigned int Length () const
    {
        return length;
    }

    void SetBase (unsigned int newBase)
    {
        base = newBase;
    }
    void SetLength (unsigned int newLength)
    {
    T* const newData = new T [newLength];
    unsigned int const min =
	length < newLength ? length : newLength;
    for (unsigned int i = 0; i < min; ++i)
	newData [i] = data [i];
    delete [] data;
    data = newData;
    length = newLength;
    }

};

int main()
{
    Array<int> A();
    Array<float> B();
    Array<char> C();
    return 0;
}


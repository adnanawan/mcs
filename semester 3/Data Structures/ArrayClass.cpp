#include <iostream>
using namespace std;


class Array{
	private:
		int size;
		int * ary = new int[size];
	public:
		Array():size(0){
		}
		Array(int s): size(s){
		}
		void set_size(int s){
			size = s;
		}
		int get_size(){
			return size;
		}
		int get_index_value(int i){
			return ary[i];
		}
		void set_index_value(int index, int value){
			ary[index] = value;
		}
		void sort_ascending(){
			int temp, flag = 1;
			for(int i=0; (i < size) && flag; i++){
				flag = 0;
				for(int j=0; j < size-1; j++){
					if (ary[j] > ary[j+1]){
						temp = ary[j];
						ary[j] = ary[j+1];
						ary[j+1] = temp;
						flag = 1;
					}
				}
			}
		}
		void sort_descending(){
			int temp, flag = 1;
			for(int i=0; (i < size) && flag; i++){
				flag = 0;
				for(int j=0; j < size-1; j++){
					if (ary[j] < ary[j+1]){
						temp = ary[j];
						ary[j] = ary[j+1];
						ary[j+1] = temp;
						flag = 1;
					}
				}
			}
		}
		void display(){
			for(int i=0; i < size; i++){
				cout << ary[i] << endl;
			}
			cout << endl;
		}
	~Array(){
		delete []ary;
		ary = 0;
	}
};

int main(){

	Array ary(5);
	ary.set_index_value(4, 2);
	ary.set_index_value(0, 3);
	ary.set_index_value(1, 100);
	ary.set_index_value(2, 90);
	ary.set_index_value(3, 30);
	ary.display();
	ary.sort_ascending();
	cout << "Ascending sort " << endl;
	ary.display();
	ary.sort_descending();
	cout << "Descending sort " << endl;
	ary.display();


	Array ary1(10);
	ary1.set_index_value(6, 45);
	ary1.display();

	return 0;
}


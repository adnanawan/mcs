/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Adnan Awan
 *
 * Created on November 20, 2015, 3:09 AM
 */

#include <iostream>
using namespace std;

class Array{
	
	private:
		int *p;
		int size, position, value;
		
	public:
		Array(int s){
			set_size(s);
			p = new int[size];
		}
		
		void set_size(int s){
			size = s;
		}
		int get_size(){
			return size;
		}
		void set_position(int p){
			position = p;
		}
		int get_position(){
			return position;
		}
		void set_value(int v){
			value = v;
		}
		
		int get_value(){
			return value;
		}
		
		void insert(int position, int value){
			p[position - 1] = value;
		}
		
		void update(int position, int value){
			int j = size;
			size ++;
			while(j >= position){
				p[j] = p[j-1];
				j--;
			}
			p[position - 1] = value;
		}
		
		bool remove(int value){
			int i;
			if(search(value)){
				i = position - 1;
				while(i < size){
					p[i] = p[i + 1];
					i++;
				}
				size --;
				return true;	
			}
			return false;
		}
		
		bool search(int value){
			int i = 0;
			while(i < size){
				if (p[i] == value){
					position = i+1;
					return true;
				}
				i++;
			}
			return false;
		}
		
		void display(){
			for (int i = 0; i < size; i++){
				cout << p[i] << endl;
			}
		}
		
		~Array(){
			delete []p;
			p = 0;
		}
};

int main(){

	int size = 6;
	Array a(size);
	
	for (int i = 1; i <=size; i++){
		a.insert(i, i * 2);
	}
	
	a.display();
	
	if(a.search(18)){
		cout << "Found" << endl;
	}
	else{
		cout << "Not found" << endl;
	}
	
	a.update(3, 16);

	a.display();
	
	cout << "Removing .." << endl;
	
	a.remove(2);
	
	a.display();
	
	a.update(1, 100);
	
	cout << "inserting" << endl;
	
	a.display();
        
        a.remove(6);
        
        a.display();
	return 0;
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FivegamesAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 3, greatorThanAverageAge = 0;
            CricketPlayer[] c = new CricketPlayer[size];
            FootBallPlayer[] f = new FootBallPlayer[size];
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine("Please enter the cricket player " + (i + 1) + " information accordingly\n1. Enter player name\n2. Enter Player DOB\n3. Enter no of matches he/she played\n4. Enter the player roll\n5. Enter the total score");
                c[i] = new CricketPlayer(Console.ReadLine(), Console.ReadLine(), Int32.Parse(Console.ReadLine()), Console.ReadLine(), Int32.Parse(Console.ReadLine()));
            }

            for (int i = 0; i < size; i++)
            {
                Console.WriteLine("Please enter the football player " + (i + 1) + " information accordingly\n1. Enter player name\n2. Enter Player DOB\n3. Enter no of matches he/she played\n4. Enter the player position\n5. Enter the total goals");
                f[i] = new FootBallPlayer(Console.ReadLine(), Console.ReadLine(), Int32.Parse(Console.ReadLine()), Console.ReadLine(), Int32.Parse(Console.ReadLine()));
            }


            foreach (CricketPlayer c1 in c)
            {

                if (c1.p.calculate_age().Year - 1 > Player.get_overAllAverageAge().Year)
                {
                    greatorThanAverageAge++;
                }
            }
            foreach (FootBallPlayer f1 in f)
            {

                if (f1.p.calculate_age().Year - 1 > Player.get_overAllAverageAge().Year)
                {
                    greatorThanAverageAge++;
                }
            }
            Console.WriteLine("The players who's ages are > average age: " + greatorThanAverageAge);
            Console.WriteLine("Cricket average score are: " + CricketPlayer.get_average_score());
            Console.WriteLine("Cricketers average age is: " + CricketPlayer.get_average_cricketers_age());
            Console.WriteLine("Football average goals are: " + FootBallPlayer.get_average_goals());
            Console.WriteLine("Football average age is: {0} ", FootBallPlayer.get_average_age());
            Console.WriteLine("Overall Average age is {0} Days, {1} Month(s), {2} Year(s).", Player.get_overAllAverageAge().Day, Player.get_overAllAverageAge().Month, (Player.get_overAllAverageAge().Year - 1));
            Console.ReadLine();// Pause Console to see out put.
        }
    }

    /// <summary>
    /// It's a player class and it has a composition relationship with CricketPlayer and FootballPlayer classes.
    /// </summary>

    class Player
    {
        DateTime currentDate;
        private static int day;
        private static int month;
        private static int year;
        private string name;
        private DateTime age;
        private int noOfMatches;
        private DateTime dateOfBirth;
        private static double overAllTotalAge;
        private static int totalPlayers;

        public Player()
        {
            name = null;
            noOfMatches = 0;
            currentDate = DateTime.Now;
            totalPlayers++;
        }

         public Player(string name, string DOB, int noOfMatches)
        {
            set_name(name);
            set_dateOfBirth(DOB);
            set_noOfMatches(noOfMatches);
            currentDate = DateTime.Now;
        }

        public DateTime calculate_age()
        {
            return new DateTime(currentDate.Subtract(dateOfBirth).Ticks);
        }

        public DateTime get_age()
        {
            return currentDate.AddDays(-(currentDate - dateOfBirth).TotalDays);
        }

        public void set_name(string name)
        {
            this.name = name;
            totalPlayers++;
        }

        public void set_dateOfBirth(string DOB)
        {
            if (!DOB.Equals(""))
            {
                dateOfBirth = DateTime.Parse(DOB);
                age = get_age();
                year += age.Year;
                month += age.Month;
                day += age.Day;
                overAllTotalAge += (currentDate - dateOfBirth).TotalDays;
            }
            
        }

        public string get_name()
        {
            return name;
        }
        public DateTime get_dateOfBirth()
        {
            return dateOfBirth;
        }

        public void set_noOfMatches(int noOfMatches)
        {
            this.noOfMatches = noOfMatches;
        }
        public int get_noOfMatches()
        {
            return noOfMatches;
        }

        public static int get_total_players()
        {
            return totalPlayers;
        }
        public static DateTime get_overAllAverageAge()
        {
            DateTime currentDate = DateTime.Now;
            DateTime d = new DateTime((int)(year / (float)totalPlayers), (int)(month / (float)totalPlayers), (int)(day / (float)totalPlayers));
            return new DateTime(currentDate.Subtract(d).Ticks);
        }
    }

    class CricketPlayer
    {
        private static int totalScore;
        private string roll;
        private int score;
        public Player p;
        private static int totalCricketersAge;
        private static int totalPlayers;

        //public CricketPlayer()
        //{
        //    roll = null;
        //    score = 0;
        //    p = null;
        //    totalScore = 0;
        //    totalPlayers++;
        //}
        public CricketPlayer(string name, string DOB, int noOfMatches, string roll, int score)
        {
            set_player_roll(roll);
            set_score(score);
            p = new Player(name, DOB, noOfMatches);
            totalCricketersAge += new DateTime(DateTime.Now.Subtract(p.get_dateOfBirth()).Ticks).Year - 1;
            totalPlayers++;
        }
        public void set_player_roll(string playerRoll)
        {
            roll = playerRoll;
        }

        public string get_player_roll()
        {
            return roll;
        }

        public void set_score(int score)
        {
            this.score = score;
            totalScore+=this.score;
        }

        public int get_score()
        {
            return score;
        }

        public void display_profile()
        {
            Console.WriteLine("Name: " + p.get_name() + 
                "\nAge: " + p.get_age() + 
                "\nNo of Matches Played: " + p.get_noOfMatches() + 
                "\nDOB: " + p.get_dateOfBirth()
                
                );
        }

        public static int get_total_players()
        {
            return totalPlayers;
        }

        public int get_total_score()
        {
            return totalScore;
        }
        public static float get_average_score()
        {
            return totalScore / (float)totalPlayers;
        }
        public static float get_average_cricketers_age()
        {
            return totalCricketersAge / (float)totalPlayers;
        }

    }

        /// <summary>
        /// Football class
        /// </summary>

    class FootBallPlayer
    {
        private static int totalGoals;
        private string position;
        private int goal;
        private static int totalAge;
        public Player p;
        private static int totalPlayers;

        //public FootBallPlayer()
        //{
        //    position = null;
        //    goal = 0;
        //    p = null;
        //    totalGoals = 0;
        //    totalPlayers++;
        //}
        public FootBallPlayer(string name, string DOB, int noOfMatches, string position, int goal)
        {
            p = new Player(name, DOB, noOfMatches);
            set_player_position(position);
            set_goal(goal);
            totalAge += new DateTime(DateTime.Now.Subtract(p.get_dateOfBirth()).Ticks).Year - 1;
            totalPlayers++;
        }
        public void set_player_position(string playerPosition)
        {
            position = playerPosition;
        }

        public string get_player_position()
        {
            return position;
        }

        public void set_goal(int goal)
        {
            this.goal = goal;
            totalGoals += this.goal;
        }

        public int get_goal()
        {
            return goal;
        }

        public void display_profile()
        {
            Console.WriteLine("Name: " + p.get_name() +
                "\nAge: " + p.get_age() +
                "\nNo of Matches Played: " + p.get_noOfMatches() +
                "\nDOB: " + p.get_dateOfBirth()

                );
        }

        public static int get_total_players()
        {
            return totalPlayers;
        }

        public int get_total_goals()
        {
            return totalGoals;
        }
        public static float get_average_goals()
        {
            return totalGoals / (float)totalPlayers;
        }

        public static float get_average_age()
        {
            return totalAge / (float)totalPlayers;
        }

    }

}

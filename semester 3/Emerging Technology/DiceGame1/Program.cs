﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceGame1
{
    class Program
    {
        static void Main(string[] args)
        {
            //int player1, player2;
            //player1 = get_player_value("One");
            //player2 = get_player_value("Two");
            //if(player1 == player2)
            //{
            //    Console.WriteLine("Player one wins. ");
            //}
            //else
            //{
            //    Console.WriteLine("Player two wins. ");
            //}

            DiceGame d = new DiceGame();
            int p1, p2;
            while (!d.get_winer()) {

                // Player one

                d.set_dice_value(d.throw_dice("One"));
                p1 = d.get_dice_value() + d.get_player1_position();
                if (p1 > 100)
                {
                    p1 = p1 - d.get_dice_value();
                    d.set_player1_position(p1);
                }
                else if (p1 == 100)
                {
                    Console.WriteLine("Player one wins.");
                    d.set_winer(true);
                    break;
                }
                else if (d.is_ladder_exist(p1))
                {
                    p1 = p1 + 5;
                    d.set_player1_position(p1);
                }
                else if (d.is_snake_exist(p1))
                {
                    p1 = p1 - 15;
                    d.set_player1_position(p1);
                }
                d.set_player1_position(p1);
                Console.WriteLine("Player one position is " + p1);

                // Second player

                d.set_dice_value(d.throw_dice("Two"));
                p2 = d.get_dice_value() + d.get_player2_position();
                if (p2 > 100)
                {
                    p2 = p2 - d.get_dice_value();
                    d.set_player2_position(p2);
                }
                else if (p2 == 100)
                {
                    Console.WriteLine("Player Two wins.");
                    d.set_winer(true);
                    break;
                }
                else if (d.is_ladder_exist(p2))
                {
                    p2 = p2 + 5;
                    d.set_player2_position(p2);
                }
                else if (d.is_snake_exist(p2))
                {
                    p2 = p2 - 15;
                    d.set_player2_position(p2);
                }
                Console.WriteLine("Player second position is " + p2);
                d.set_player2_position(p2);
            }
            
            Console.Read();
        }

        public static int get_player_value(string p){
            int value;
            do
            {
                Console.Write("Enter the dice number from 1 to 6 inclusive for player " + p + " : ");
                value = Int32.Parse(Console.ReadLine());
            } while (value < 1 || value > 6);
            return value;
        }

    }

    class DiceGame
    {
        private bool win;
        private int player1, player2, dice;

        public DiceGame()
        {
            win = false;
            player1 = 0;
            player2 = 0;
            dice = 0;
        }

        public void set_winer(bool w)
        {
            win = w;
        }
        public bool get_winer()
        {
            return win;
        }
        public void set_player1_position(int p)
        {
            player1 = p;
        }
        public void set_player2_position(int p)
        {
            player2 = p;
        }
        public int get_player1_position()
        {
            return player1;
        }
        public int get_player2_position()
        {
            return player2;
        }
        public int throw_dice(string p)
        {
            int value;
            do
            {
                Console.Write("Enter the dice number from 1 to 6 inclusive for player " + p + " : ");
                value = Int32.Parse(Console.ReadLine());
            } while (value < 1 || value > 6);
            return value;
        }

        public bool is_ladder_exist(int value)
        {
            if (value == 5 || value == 19 || value == 63 || value == 79){
                Console.WriteLine("+5");
                return true;
            }
            return false;
        }

        public bool is_snake_exist(int value)
        {
            if (value == 25 || value == 43 || value == 69 || value == 92)
            {
                Console.WriteLine("-15");
                return true;
            }
            return false;
        }

        public void set_dice_value(int value)
        {
            dice = value;
        }
        public int get_dice_value()
        {
            return dice;
        }
    }
}

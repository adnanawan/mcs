using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {

        static void Main(string[] args)
        {

            Game g = new Game();
            int p1 = 1, p2 = 2;
            bool w = true;
            while (w)
            {
                g.get_player_input("one");
                g.set_input(p1);
                g.display_game_menu();
                if (g.is_row_match() || g.is_col_match() || g.is_diagonal_match())
                {
                    Console.WriteLine("Player one wins.");
                    w = false;
                }
                else if (g.is_draw())
                {
                    Console.WriteLine("Game has been drawn.");
                    w = false;
                }
                else
                {
                    g.get_player_input("Two");
                    g.set_input(p2);
                    g.display_game_menu();
                    if (g.is_row_match() || g.is_col_match() || g.is_diagonal_match())
                    {
                        Console.WriteLine("Player two wins.");
                        w = false;
                    }
                    else if (g.is_draw())
                    {
                        Console.WriteLine("Game has been drawn.");
                        w = false;
                    }
                }
            }

            Console.ReadLine();
        }
    }


    class Game
    {
        private int row, col;
        private int[,] gameArray;

        public Game()
        {
            gameArray = new int[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    gameArray[i, j] = 0;
                }
            }

        }

        public void display_game_menu()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("\t -----------");
            Console.WriteLine("\t| " +gameArray[0, 0] + " | " + gameArray[0, 1] + " | " + gameArray[0, 2] + " |");
            Console.WriteLine("\t| " + gameArray[1, 0] + " | " + gameArray[1, 1] + " | " + gameArray[1, 2] + " |");
            Console.WriteLine("\t| " + gameArray[2, 0] + " | " + gameArray[2, 1] + " | " + gameArray[2, 2] + " |");
            Console.WriteLine("\t -----------");
        }

        public bool is_draw()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gameArray[i, j] == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public int get_player_input(string p)
        {
            int a;
            bool flag;
            do
            {
                Console.Write("Enter the location from 1-9 inclusive for player " + p + ": ");
                a = Int32.Parse(Console.ReadLine());
                if (!is_location_available(a) && (a > 0 && a < 10))
                {
                    Console.WriteLine("Entered location isn't available.");
                    a = 100;
                }

            } while (a < 1 || a > 9);

            return a;

        }

        public void set_input(int value)
        {
            gameArray[row, col] = value;
        }
        public bool is_location_available(int c)
        {
            switch (c)
            {
                case 1:
                    {
                        row = 0;
                        col = 0;
                        break;
                    }
                    
                case 2:
                    {
                        row = 0;
                        col = 1;
                        break;
                    }
                    
                case 3:
                    {
                        row = 0;
                        col = 2;
                        break;
                    }
                case 4:
                    {
                        row = 1;
                        col = 0;
                        break;
                    }
                case 5:
                    {
                        row = 1;
                        col = 1;
                        break;
                    }
                    
                case 6:
                    {
                        row = 1;
                        col = 2;
                        break;
                    }
                    
                case 7:
                    {
                        row = 2;
                        col = 0;
                        break;
                    }
                    
                case 8:
                    {
                        row = 2;
                        col = 1;
                        break;
                    }
                    
                case 9:
                    {
                        row = 2;
                        col = 2;
                        break;
                    }
                    
                default:
                    return false;
            }
            return gameArray[row, col] == 0;
        }
        public bool is_row_match()
        {
            for (int i = 0; i < 3; i++)
            {
                if (((gameArray[i, 0] == gameArray[i , 1]) && (gameArray[i, 0] == gameArray[i, 2])) && gameArray[i, 0] != 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool is_col_match()
        {
            for (int i = 0; i < 3; i++)
            {
                if (((gameArray[0, i] == gameArray[1, i]) && (gameArray[0, i] == gameArray[2, i])) && gameArray[0, i] != 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool is_diagonal_match()
        {
            if((gameArray[0, 0] == gameArray[1,1]) && (gameArray[0, 0] == gameArray[2,2]) && gameArray[0, 0] != 0)
            {
                return true;
            }
            else if((gameArray[0, 2] == gameArray[1, 1]) && (gameArray[0, 2] == gameArray[2, 0]) && gameArray[0, 2] != 0)
            {
                return true;
            }
            return false;
        }
    }
}

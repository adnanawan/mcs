<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Php practice page</title>
    </head>
    <body id='bdy'>
        <script type="text/javascript">
            var i = 1;
            function onClick(){
                //document.getElementsByTagName('body')[0].style.background = 'red';
                var node = document.getElementById('colorChange');
                node.style.display = 'block';
                node.innerHTML += '<p>' + i + '</p>';
                switch(i){
                    case 1:{
                        node.style.color = 'orange';
                        break;
                    }
                    case 2:{
                        node.style.color = 'green';
                        break;
                    }
                    case 3:{
                        node.style.color = 'gray';
                        break;
                    }
                    case 4:{
                        node.style.color = 'pink';
                        break;
                    }
                    case 5:{
                        node.style.color = 'red';
                        break;
                    }
                    case 6:{
                        node.style.color = 'yellow';
                        break;
                    }
                    default:{
                        node.style.background = 'black';
                        node.style.color = 'white';
                        break;
                    }
                    
                }
                i++;
//                while (i <= 10){
//                    //document.write(i);
//                    node.innerHTML += ('<p>' + i + '</p>');
//                    i++;
//                }
            }
            
            
            function clearNode(){
                var node = document.getElementById('colorChange');
                node.innerHTML = "<p>It's a new div in html.</p>";
                node.style.background = 'white';
                node.style.display = 'none';
                i = 1;
            }
        </script>
        <?php
        // put your code here
        $names = array('Adnan', 'Muhammad', 'Ahmad', 'Armaan', 'Usman');
        $i = 0;
        while ($i < sizeof($names)){
            echo $names[$i++].'<br/>';
        }
        ?>
        <div id='colorChange'>
            <p>It's a new div in html.</p>
        </div>
        <ul style="list-style: none;">
            <li style="display: inline;"><input type="submit" onclick="onClick();" value="ADD"></li>
            <li style="display: inline;"><input type="submit" onclick="clearNode();" value="CLEAR"></li>
        </ul>
        
    </body>
</html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Php form</title>
        <style>
            ul{
                list-style-type: none;
            }
            #check, #radio1, #radio2, #textarea, #textareaLable{
                display: inline;
            }
            textarea{
                width: 250px;
                height: 150px;
            }
        </style>
        <script type="text/javascript">
            function formValidation(){
                var temp = document.forms['myForm']['user'].value.match(/^[\w\.]{5,10}@[\w\.]{4,20}$/);
                if (temp == null || temp == "") {
                    alert("Please enter valid email-id");
                    return false;
                }
                temp = document.forms['myForm']['pass'].value.match(/^[\w]{8,15}$/);
                if (temp == null || temp == "") {
                    alert("Password length must be 8-15.");
                    return false;
                }
                temp = document.forms['myForm']['like'].value;
                var temp1 = document.forms['myForm']['gender'].value;
                if (temp == 'py' && temp1 == 'female') {
                    alert("Male can only choose python.");
                    return false;
                }
            }
        </script>
    </head>
    
    <body>
        <form name="myForm" method="POST" action="" onsubmit="return formValidation()">
            <ul>
                <li id="un">
                    <label>User Name: </label>
                    <input type="text" name="user" value="" placeholder="username" required=""/>
                </li>
                <li id="pwd">
                    <label>Password: </label>
                    <input type="password" name="pass" placeholder="password" required=""/>
                </li>
                <li id='textareaLable'>
                    <label>Text Area: </label>
                </li>
                <li id="textarea">
                    <textarea placeholder="Write something about programming."></textarea>
                </li>
                <li id="choice">
                    Do you like?
                </li>
                <li id="check">
                    <input type="checkbox" name="box" value="htmlcss"/>
                    <label>HTML & CSS </label>
                </li>
                <li id="radio1">
                    <input type="radio" name="like" value="php"/>
                    <label>PHP </label>
                </li>
                <li id="radio2">
                    <input type="radio" name="like" value="py"/>
                    <label>Python </label>
                </li>
                <li id="gen">
                    <select name="gender">
                        <option value="male"> Male </option>
                        <option value="female"> Female </option>
                    </select>
                </li>
                <li id="submit">
                    <input type="submit"/>
                </li>
            </ul>
        </form>
    <?php
    if($_POST){
        echo "Entered user name is: ".$_POST['user'].'<br/>';
        echo "Entered password is: ".$_POST['pass'].'<br/>';
        if(array_key_exists('like', $_POST)){
            if($_POST['like'] == 'py'){
                echo "You like Python".'<br/>';
            }
            else if($_POST['like'] == 'php'){
                echo 'You like PHP'.'<br/>';
            }
        }

        if($_POST['gender'] == 'male'){
            echo 'You are male</br>';
        }
        else{
            echo 'You are female<br/>';
        }
        if(array_key_exists('box', $_POST)){
            echo 'You also like HTML and CSS'.'<br/>';
        }
    }
    ?>        
        
    </body>
</html>
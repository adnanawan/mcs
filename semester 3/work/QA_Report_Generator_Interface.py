import tkMessageBox
from Tkinter import *


class QARGInterface:

    main = None
    key = None
    button = None
    start_date = None
    end_date = None
    row_no = None

    def __init__(self, title):
        self.main_interface(title)

    def main_interface(self, title):
        self.main = Tk()
        self.main.title(title)

    def intranet_authentication_key(self, m):
        l = Label(m, text="Intranet Access Key")
        l.grid(row=1)
        e = Entry(m, bd=5, width=50)
        e.grid(row=1, column=1)
        self.key = e

    def confirm_button(self):
        b = Button(self.main, text="Confirm", command=self.confirm, width=20)
        b.grid(row=5, column=0)

    def confirm(self):
        if self.key.get() and self.start_date.get() and self.end_date.get() and self.row_no.get():
            self.button.config(state="normal")
            return
        tkMessageBox.showinfo("Missing field(s)", "Please fill all the fields")

    def submit(self):
        b = Button(self.main, text="Send", command=self.get_key,width=20, state=DISABLED)
        b.grid(row=5, column=1)
        self.button = b

    def get_key(self):
        tkMessageBox.showinfo('Key', self.key.get() + " " + self.start_date.get())

    def start_date(self):
        sl = Label(self.main, text='Start date')
        sl.grid(row=2)
        sd = Entry(self.main, width=50, bd=5)
        sd.grid(row=2, column=1)
        self.start_date = sd

    def end_date(self):
        el = Label(self.main, text='End date')
        el.grid(row=3)
        ed = Entry(self.main, width=50, bd=5)
        ed.grid(row=3, column=1)
        self.end_date = ed

    def row_number(self):
        rl = Label(self.main, text='Sheet Row Number')
        rl.grid(row=4)
        row = Entry(self.main, width=50, bd=5)
        row.grid(row=4, column=1)
        self.row_no = row

if __name__ == '__main__':

    instance = QARGInterface("QA Report Generator")
    instance.intranet_authentication_key(instance.main)
    instance.start_date()
    instance.end_date()
    instance.row_number()
    instance.confirm_button()
    instance.submit()
    instance.main.mainloop()